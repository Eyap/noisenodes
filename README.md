# Noise Nodes
Various noise nodes for Unity and ShaderGraph.

The noise nodes are mainly importation of the hlsl-noise inside Unity ShaderGraph. See: https://gitlab.com/Eyap/hlsl-noise.

## Recommanded installation : 

You can be install this package via the Unity Package Manager (UPM). In the top left of the Packages window, navigate to Add Package -> Add package from git URL and paste [https://gitlab.com/Eyap/noisenodes.git](https://gitlab.com/Eyap/noisenodes.git).

## Note for myself :

I made some improvements compared to : https://github.com/JimmyCushnie/Noisy-Nodes (particularly in the hlsl-noise used).
Since then, there is also new updates and discovery in the noise (see https://github.com/stegu/webgl-noise/). TODO : Create a fork of the JimmyCushnie's repo and PR the updates.

## License

Except for the hlsl-noise library provided under the MIT License, all the files are released under the MPL-2.0. See [LICENSE.md](./LICENSE.md).
